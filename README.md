# Lib-user

## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it
easy? [Use the template at the bottom](#editing-this-readme)!

## Command line instructions

You can also upload existing files from your computer using the instructions below.

## Git global setup

```
git config --global user.name "Khoem Sombath"
git config --global user.email "sombath.sb13@gmail.com"
```

## Create a new repository

```
git clone git@gitlab.com:khoem.sombath/ossrh-83842.git
cd lib-user
git switch -c main
touch README.md
git add README.md
git commit -m "add README"
git push -u origin main
```

## Push an existing folder

```
cd existing_folder
git init --initial-branch=main
git remote add origin git@gitlab.com:khoem.sombath/ossrh-83842.git
git add .
git commit -m "Initial commit"
git push -u origin main
```

## Push an existing Git repository

```
cd existing_repo
git remote rename origin old-origin
git remote add origin git@gitlab.com:khoem.sombath/ossrh-83842.git
git push -u origin --all
git push -u origin --tags
```