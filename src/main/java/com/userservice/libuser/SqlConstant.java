package com.userservice.libuser;

/**
 * @author Sombath
 * create at 16/6/22 11:58 AM
 */
public class SqlConstant {

    public final static String LIMIT = " limit ";
    public final static String OFFSET = " offset ";
    public final static String FROM = "from ";
    public final static String GROUP = "group ";
    public final static String ORDER = "order ";

    public final static String COUNT_QUERY = "select count(*) ";
    public final static String COUNT_DISTINCT_QUERY = "select count(distinct *) ";
    public static final String EMPTY_STRING = "";

}
